## About

BookStack is an opinionated wiki system that provides a pleasant and simple out
of the box experience. New users to an instance should find the experience intuitive
and only basic word-processing skills should be required to get involved in creating
content on BookStack.

## Feature

* Free & Open Source
* Easy, Simple Interface
* Configurable
* Multi-lingual
* Integrated Authentication

