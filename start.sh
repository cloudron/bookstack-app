#!/bin/bash

set -eu

mkdir -p /run/bookstack/bootstrap-cache /run/bookstack/sessions /run/bookstack/framework-cache /run/bookstack/logs /app/data/themes

readonly ARTISAN="sudo -E -u www-data php /app/code/artisan"

if [[ ! -f /app/data/env ]]; then
    echo "=> Detected first run"

    # set up default directories with write access and copy the data from readonly
    mkdir -p /app/data/uploads /app/data/storage
    cp /app/pkg/env.production /app/data/env
    cp -r /app/code/storage_orig/* /app/data/storage

    # generate key & migrate db
    chown -R www-data:www-data /run/bookstack /app/data
    $ARTISAN key:generate --no-interaction --force
    $ARTISAN migrate --no-interaction --force

    # disable registration and do not require email confirmation. set default role to 'admin'
    readonly mysql_cli="mysql -u${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} -h ${CLOUDRON_MYSQL_HOST} -P ${CLOUDRON_MYSQL_PORT} ${CLOUDRON_MYSQL_DATABASE}"
    $mysql_cli -e "INSERT INTO settings (setting_key, value, created_at, updated_at) VALUES ('registration-confirmation', 'false', NOW(), NOW());"
    $mysql_cli -e "INSERT INTO settings (setting_key, value, created_at, updated_at) VALUES ('registration-enabled', 'false', NOW(), NOW());"
    $mysql_cli -e "INSERT INTO settings (setting_key, value, created_at, updated_at) VALUES ('registration-restrict', '', NOW(), NOW());"
    $mysql_cli -e "INSERT INTO settings (setting_key, value, created_at, updated_at) VALUES ('registration-role', '1', NOW(), NOW());"

    [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]] && sed -e "s/AUTH_METHOD=.*/AUTH_METHOD=oidc/" -i /app/data/env
else
    echo "=> Existing installation. Running migration script"
    chown -R www-data:www-data /run/bookstack /app/data
    $ARTISAN migrate --no-interaction --force

    if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        echo "=> Ensure OIDC config"
        sed -e "s,OIDC_ISSUER=.*,OIDC_ISSUER=${CLOUDRON_OIDC_ISSUER}," \
            -e "s/OIDC_CLIENT_SECRET=.*/OIDC_CLIENT_SECRET=${CLOUDRON_OIDC_CLIENT_SECRET}/" \
            -e "s/OIDC_CLIENT_ID=.*/OIDC_CLIENT_ID=${CLOUDRON_OIDC_CLIENT_ID}/" \
            -i /app/data/env
    fi
fi

[[ -z "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-}" ]] && export CLOUDRON_MAIL_FROM_DISPLAY_NAME=BookStack
[[ -z "${CLOUDRON_OIDC_PROVIDER_NAME:-}" ]] && export CLOUDRON_OIDC_PROVIDER_NAME=Cloudron

# sessions, logs and cache
[[ -d /app/data/storage/framework/sessions ]] && rm -rf /app/data/storage/framework/sessions
ln -sf /run/bookstack/sessions /app/data/storage/framework/sessions
rm -rf /app/data/storage/framework/cache && ln -s /run/bookstack/framework-cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/bookstack/logs /app/data/storage/logs

# migrating & clearing cache after update
$ARTISAN cache:clear
$ARTISAN view:clear

echo "=> Run BookStack"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

