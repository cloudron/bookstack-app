#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;
    const MANIFEST = require('../CloudronManifest.json');

    let app;
    let browser;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement (elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function loginOIDC(username, password, alreadyAuthenticated) {
        browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + "/login");
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//button[@id="oidc-login"]')).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.xpath('//input[@name="username"]'));
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);
        }

        await waitForElement(By.xpath('//h3[text()="Recent Activity"]'));
    }

    async function loginLocal(username, password) {
        const id = username.includes('@') ? 'email' : 'username';
        browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + "/login");
        await waitForElement(By.id(id));
        await browser.findElement(By.id(id)).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Log In"]')).click();
        await waitForElement(By.xpath('//h3[text()="Recent Activity"]'));
    }


    async function createBook() {
        await browser.get('https://' + app.fqdn + '/create-book');
        await waitForElement(By.id('name'));
        await browser.findElement(By.id('name')).sendKeys('cloudron-book');
        await browser.findElement(By.xpath('//label[contains(text(), "Cover image")]')).click();
        await browser.findElement(By.id('image')).sendKeys(path.resolve(__dirname, '../logo.png'));
        const button = browser.findElement(By.xpath('//button[text()="Save Book"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.sleep(4000);
        await browser.findElement(By.xpath('//button[text()="Save Book"]')).click();
        await waitForElement(By.xpath('//p[contains(text(),"No pages or chapters have been created for this book")]'));
    }

    async function checkBook() {
        await browser.get('https://' + app.fqdn + '/books/cloudron-book');
        await waitForElement(By.xpath('//p[contains(text(),"No pages or chapters have been created for this book")]'));
    }

    async function checkBookImage() {
        await browser.get('https://' + app.fqdn + '/books/cloudron-book/edit');
        await browser.findElement(By.xpath('//label[contains(text(), "Cover image")]')).click();
        await waitForElement(By.xpath('//img[contains(@src, "logo.png")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no sso
    it('can install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login', loginLocal.bind(null, 'admin@admin.com', 'password'));
    it('can create book', createBook);
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // sso
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false));
    it('can create book', createBook);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check book', checkBook);
    it('can check book image ', checkBookImage);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check book', checkBook);
    it('can check book image ', checkBookImage);

    it('move to different location', function () { execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check book', checkBook);
    it('can check book image ', checkBookImage);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app for update', function () { execSync('cloudron install --appstore-id ' + MANIFEST.id + ' --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can create book', createBook);

    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check book', checkBook);
    it('can check book image ', checkBookImage);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
