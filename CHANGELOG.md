[0.1.0]
* Initial commit

[0.1.1]
* Added redis config but disabled, apparently we also need a username
* Updated to 0.25.5

[0.1.2]
* Updated to 0.26.1

[0.1.3]
* Updated to 0.26.2

[0.1.4]
* Updated manifestVersion to get LDAP working properly

[0.1.5]
* Automatically add settings for LDAP login

[0.1.6]
* Temporarily removed migrations

[0.1.7]
* Update to v0.26.3

[0.2.0]
* Fixup post install message

[0.3.0]
* Enable secure cookies

[0.4.0]
* Update BookStack to 0.26.4
* This is a security release
* Updated user profile behaviour so that users cannot change their email address unless they have permission to manage users. This is to prevent a user acting as an imposter, changing their email to one they don't own. Thanks to @Irrational-NX for raising.
* Improved the script escaping logic that was enhanced in the previous release, by also checking for iframes using javascript or data urls. Thanks again to @billford for raising this issue. (#1531)
* Updated the provided, and added an additional, .htaccess file to prevent apache indexes from listing image directories. Thanks to @davidtessier for raising.

[1.0.0]
* Update BookStack to 0.27.2
* Reviewed accessibility of BookStack to move towards WCAG 2.0 Support. (#1320, #1476)
* Added page templating functionality. (#129, #1527)
* Added the ability to send a new user a sign-up link where the user can set their own password. (#316)
* Added the ability to set seperate storage types for Images and Attachments. (#1302)
* Added notice to the "Custom HTML Head Content" setting to advise it does not apply while on the settings page. (#1144)
* Updated entity permissions table so it's hidden unless custom permissions are enabled to prevent confusion. Thanks to @timoschwarzer. (#1505)

[1.1.0]
* Update BookStack to 0.27.3
* Fixed issue where images could not be pasted due to incorrect handling of FormData requests. (#1621)

[1.2.0]
* Update BookStack to 0.27.4
* Fixes UI issue that make book quick-sort options not be clickable. (#1639)
* Updated Russian translations. Thanks to @kostefun. (#1637, #1636, #1635, #1630, #1629, #1628, #1627, #1626, #1624)
* Updated Spanish translations. Thanks to @moucho. (#1633)
* Updated 'Spanish Argentina' translations. Thanks to @leomartinez. (#1623)

[1.2.1]
* Update BookStack to 0.27.5

[1.3.0]
* Add optional sso support

[1.4.0]
* Update Bookstack to 0.28.0

[1.4.1]
* Update Bookstack to 0.28.2

[1.4.2]
* Update Bookstack to 0.28.3

[1.5.0]
* Update Bookstack to 0.29.0
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v0.29.0)
* Added a user-selectable dark-mode option. (#2022, #1234)
* Added the ability to define a custom draw.io URL and therefore use a custom instance if preferred. (#826)
* Added grid-view support, with toggle, to the shelf view. Thanks to @philjak. (#1755, #1221)
* Added a list of bookshelves that a book belongs when viewing a book. Thanks to @cw1998. (#1688, #1598)
* Added a new command to update your BookStack URL in the database. (#1225)
* Added shelf API endpoints. Thanks to @osmansorkar. (#1908)
* Added book-export API endpoints.
* Updated password reset flows to avoid indicating if a email is in use within the system. (#2016)

[1.6.0]
* Update Bookstack to 0.29.1
* Use latest base image 2.0.0
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v0.29.1)
* Added multi-item select to the book-sort interface. (#2067)
* Updated authentication system to prevent admins being logged out when changing authentication type, useful when setting up LDAP or SAML. (#2031)
* Updated editor focus so that the title is ready-selected if the default, otherwise the editor is focused. (#2036)
* Updated translations for Dutch, Korean, French, Turkish, Spanish. Thanks to Crowdin Users. (#2028, #2071)
* Fixed issue where callout styles could not be cycled through via shortcut when in-callout formatting was selected in the editor. (#2061)
* Fixed issue where the selection area was not visible in code blocks or the markdown editor when using dark mode. (#2060)

[1.6.1]
* Update Bookstack to 0.29.2
* This most impacts scenarios where not-trusted users are given permission to create comments.

[1.6.2]
* Update Bookstack to 0.29.3
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v0.29.3)
* This release addresses issue #2111 where the name of a restricted book could be viewed by non-authorised users when the book was on a shelf, and the shelves were viewed in "List View"
* Move apache out of port 80

[1.7.0]
* Fix tags
* Update postinstall
* Add forumUrl

[1.8.0]
* Update Bookstack to 0.30.0
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v0.30.0)
* Security Notice - Possible Privilege Escalation
* Added API endpoints for chapters.
* Added audit log to the settings area. (#2173, #1167)
* Added the ability to insert an attachment link directly into the current editor window. (#1460)
* Added session-based code-block editor auto-save to prevent potential loss of content. (#1398)
* Added warning wording around role system permissions to indicate what permissions could allow privilege escalation. (#2105)
* Added the ability to log login failures to a file. Thanks to @benrubson. (#1881, #728)
* Updated Simplified Chinese translations. Thanks to @Honvid. (#2157)
* Updated WYSIWYG editor css to put editor in it's own layer to improve degraded dark mode performance. (#2154)
* Updated Czech translations. Thanks to @jakubboucek. (#2238)
* Updated permission system so that the permission map table does not contain ID's since database limits could be met in scenarios where permissions were automatically refreshed on a frequent basis. (#2091)

[1.8.1]
* Update Bookstack to 0.30.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v0.30.1)
* Updated translations. (#2262)
* Updated settings header bar to adapt better for longer-text languages. (#2265)
* Updated callout link formatting to use callout text style rather than theme color. Thanks to @alexmannuk. (#2233, #303)
* Updated Book export content so that page includes are parsed. Thanks to @mr-vinn. (#2227, #2228)
* Fixed issue where the markdown editor preview pane would be empty. (#2280)
* Fixed incorrect spelling of "Ubuntu Mono" font definition. Thanks to @abulgatz. (#2274)
* Fixed incorrect AddActivityIndexes migration 'down' action. Thanks to @gertjankrol. (#2268)
* Fixed unexpected scroll bars on code blocks. (#2267)
* Fixed issue where notification would not shown upon SAML login where there's an existing non-matching user. (#2263)

[1.8.2]
* Update Bookstack to 0.30.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v0.30.2)
* Updated JavaScript build system to provide slightly better browser compatibility.
* Updated page-content save parsing to update anchor references on IDs changed by BookStack. (#2278)
* Fixed issue where creating a link attachment after mulitple validation failures would result in many duplicate links being created. (#2286)
* Updated drawing integration to, by default, use diagrams.net instead of draw.io. (#2285, #2044)
* Updated default .htaccess to align with laravel's and allow canonical redirects on non-root url app instances. Thanks to @jakubboucek. (#2272)

[1.8.3]
* Update Bookstack to 0.30.3
* Added VBScript syntax highlighting to the code block editor. Thanks to @nutsflag. (#2302, #2255)
* Fixed issue where drawings would not save in the Markdown editor. (#2313, #2321)
* Updated some Spanish and Chinese translations. (#2303)

[1.8.4]
* Update Bookstack to 0.30.4
* Various security fixes
* [Full changelog](https://www.bookstackapp.com/blog/beta-release-v0-30-4/)

[1.8.5]
* Update BookStack to 0.30.5
* [Security issues](https://www.bookstackapp.com/blog/beta-release-v0-30-5/)

[1.8.6]
* Update BookStack to 0.30.6
* [Full changelog](https://www.bookstackapp.com/blog/beta-release-v0-30-6/)

[1.8.7]
* Update BookStack to 0.30.7
* [Full changelog](https://www.bookstackapp.com/blog/beta-release-v0-30-7/)

[1.8.8]
* Add ldap uuid flag file to help migration to new username based ids

[1.9.0]
* Change the default ldap id attribute to username

[1.10.0]
* Update BookStack to 0.31.0
* [Full changelog](https://www.bookstackapp.com/blog/beta-release-v0-31-0/)
* Added recycle bin implementation. (#2283, #2183, #280)
* Added Norwegian translations to BookStack. Thanks to @Swoy. (#2336)
* Added ownership system for pages, chapters, books and shelves. (#2436, #2246)
* Added host iframe control with cookie security management. (#2427, #2207)
* Added API endpoints for pages. (#2382)
* Added many more activity types to the audit-log. (#2360, #1243)

[1.10.1]
* Update BookStack to 0.31.1
* Fixed issue where markdown content would not be stored on first page save (HTML content would still be stored). (#2446)
* Fixed issue where the new content owner fields were not be used for the manage-own-permission role permission. (#2445)
* Fixed recycle bin table style issue which could cause the dropdown menu to be cut-off. (#2442)
* Updated Chinese, Spanish and French translations. (#2441)

[1.10.2]
* Update BookStack to 0.31.3
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v0.31.3)
* Fixed issue where markdown strikethroughs were not rendering in the markdown editor. (#2470)
* Updated Turkish translations. (#2469)
* Updated some user, page and shelf views to use more efficient database querying.

[1.10.3]
* Update BookStack to 0.31.4
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v0.31.4)
* Updated framework to prevent potential security vulnerability.
* Updated Chinese Traditional translations. (#2482)

[1.10.4]
* Update BookStack to 0.31.5
* Updated laravel/framework to prevent potential security vulnerability.

[1.10.5]
* Update BookStack to 0.31.6
* Fixed issue thrown when deleting shelves from the recycle bin. Thanks to @i4j5. (#2543, #2534, #2530)
* Fixed issue where restoring a revision would restore as HTML instead of Markdown. (#2496)

[1.11.0]
* Update base image to v3
* Update PHP to 7.4

[1.11.1]
* Update BookStack to 0.31.7
* Fixed incorrect URL being used when using an s3-like file storage service. (#2603)

[1.11.2]
* Update BookStack to 0.31.8
* Fixed chapter and page book id misalignment that could occur when the page was in the recycle bin. Could cause some issues with permission generation which have also been addressed. (#2603)

[1.12.0]
* Update BookStack to 21.04
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v21.04)
* Added back-end theme system. (#2639)
* Added APP_VIEWS_BOOKSHELF .env option to set default view type within a shelf. Thanks to @philjak. (#2591)
* Added owned_by search filter. Thanks to @benediktvolke. (#2561)
* Added sorting for Books within Shelves. Thanks to @guillaumehanotel. (#2515, #1742)
* Added user filter to the Audit Log. (#2472)
* Added the ability to configure custom footer links via the settings screen. Thanks to @james-geiger. (#1973)
* Added create buttons to the books and shelves homepage view options. Thanks to @philjak. (#1756)

[1.12.1]
* Update BookStack to 21.04.1
* Updated mobile header elements for much better keyboard/screen-reader accessibility. (#2681)
* Updated translations with latest CrowdIn changes. (#2672)
* Updated WYSIWYG editor code-block handling provide a more stable undo/redo experience. (#2602)
* Updated AWS S3 SDK to fix incompatibility with Minio. (#2689)
* Fixed HTTP JSON detection when an encoding is in the response JSON content type. (#2684)

[1.12.2]
* Update BookStack to 21.04.2
* Fixed issue where a page could become inaccessible when the creator no longer existed. (#2687)
* Updated translations with latest Crowdin changes. (#2691)

[1.12.3]
* Update BookStack to 21.04.3
* Updated migration string column lengths to better fit within restrictive index limits (#2710)
* Updated select box styles with to work around default iOS styles causing issues in dark mode. (#2709)
* Updated translations with latest Crowdin changes. (#2695)
* Updated styles of layout view buttons in mobile screen sizes to respect dark mode.
* Updated image upload behaviour for s3 style uploads to set public permissions as part of the upload request instead of a separate request.
* Fixed issue where "Recently Viewed" would show non-viewed content for new users. (#2703)

[1.12.4]
* Update BookStack to 21.04.4
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v21.04.4)
* Updated translations with latest Crowdin changes. (#2719)
* Updated Korean translations. Thanks to @Jokuna. (#2716)
* Improved error messaging when attempting to access a non-existent image file. (#2696)
* Updated table style handling across exports types to be consistent. (#2666)

[1.12.5]
* Update BookStack to 21.04.5
* Fixed error during PDF export in some cases due to incorrect path. (#2746)
* Fixed error thrown when saving a markdown page with empty content. (#2741)
* Updated S3 ACL setting so ACLs are set via another request, as per pre-v21.04.2, but only when actually use AWS S3. (#2739)
* Updated translations with latest Crowdin changes. (#2737)
* Updated overflowing table content to be consistent. Thanks to @dopyrory3. (#2735, #2732)

[1.12.6]
* Update BookStack to 21.04.6
* Added a way to configure options on a social driver, for the initial redirects, through the Theme::addSocialDriver system. (#2759)
* Fixed scenario where recent Image upload visibility changes caused issues on hosting where webserver and PHP process group/user differ. (#2758)

[1.13.0]
* Update BookStack to 21.05
* Added shelf/book/chapter/page favourite system. (#2748)
* Added previous/next navigation to chapters and pages. Thanks to @shubhamosmosys. (#2511, #1381)
* Added display of tags within search results. Thanks to @burnoutberni. (#2487, #2462)
* Added the ability to import JPEG user avatar images during LDAP login/registration. 

[1.13.1]
* Update BookStack to 21.05.1
* Added base64 image extraction within page content. Thanks to @awarre. (#2700, #2631)
* Added Croatian translations. Thanks to @ffranchina. (#2784, #2785)
* Updated item permission roles list to be sorted alphabetically. (#2782)
* Merged in latest Crowdin translations. (#2787, #2777)
* Fixed incorrect styling of favourites sidebar when using a non-default homepage option. (#2783)

[1.13.2]
* Update BookStack to 21.05.2
* Added the ability to server attachments without forcing downloads. (#2791)
* Fixed issue where empty HTML comments could cause errors. (#2804)
* Updated translations with latest changes from Crowdin. (#2790)
* Extracted not found text into it's own view for easier overriding (58117bc)

[1.13.3]
* Update BookStack to 21.05.3
* Added a "Skip to content" link as first page focus item for accessibility use. (#2810)
* Updated social account detachment to have CSRF protection. (#2808)
* Updated PHP depedancy versions.
* Fixed issue where translations system may attempt to load from the root directory when a theme was not in use. (#2836)

[1.13.4]
* Update BookStack to 21.05.4
* Added VB.NET code block highlighting option. (#2869)
* Improved audit log user select list stability. (#2863)
* Fixed issue where user profile pages item "View All" links used ids hence did not link to proper searches. (#2857)

[1.14.0]
* Update BookStack to 21.08
* [Release announcement](https://www.bookstackapp.com/blog/bookstack-release-v21-08/)
* Markdown Export
* Multi-Factor Authentication
* Non-Download Attachment Links
* Role-Based Export Permissions
* “Skip to content” Link

[1.14.1]
* Update BookStack to 21.08.1
* Updated TOTP setup flow to display a URL of the QR code contents during setup for non-QR scanning usage. (#2908)
* Updated translations with latest Crowdin updates. (#2906)
* Fixed broken page ordering on various views. (#2905)

[1.14.2]
* Update BookStack to 21.08.2
* This security release is intended to cover a couple of XSS vulnerabilities
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.08.2)

[1.14.3]
* Update BookStack to 21.08.3
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.08.3)
* Fixed certain "Custom HTML Head Content" being incorrectly altered or converted. (#2923, #2914)
* Updated translations with latest Crowdin updates. (#2915)

[1.14.4]
* Update BooKStack to 21.08.4
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.08.4)
* Added IP address to tracked activities and displayed in audit log. Thanks to @johnroyer. (#2936, #2747)
* Added the option to use database table prefixes. Thanks to @floviolleau. (#2935)
* Allowed the use of content includes when using a custom homepage.
* Updated translations with latest content from Crowdin. (#2926)

[1.14.5]
* Update BookStack to 21.08.5
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.08.5)
* This security release covers a vulnerability which would allow malicious users, who have permission to update or create pages, to load content from files stored within the storage/ or public/ directories (Such as application logs) via the page HTML export system.
* Added concurrent page editing warnings upon draft save events.

[1.14.6]
* Update BookStack to 21.08.6
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.08.6)

[1.15.0]
* Update BookStack to 21.10
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.10)
* Added Attachment API endpoints. (#2986, #2942)
* Added Estonian language to BookStack via Crowdin. (#2979)
* Added support for base64 image content within markdown text via page POST/PUT. (#2898)
* Updated translations from Crowdin contributors. (#2983)
* Fixed padding within book-tree sidebar items. Thanks to @ffranchina. (#3000)

[1.15.1]
* Update BookStack to 21.10.1
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.10.1)
* Fixed image upload vulnerability. Thanks to @Haxatron (#3010)
* Fixed capitalization for Estonian language option. Thanks to @IndrekHaav. (#3008)
* Updated PHP packages to prevent abandoned warning. (#3007)
* Updated translations with latest changes from Crowdin. (#3006)

[1.15.2]
* Update BookStack to 21.10.2
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.10.2)
* Made further fixes to address image upload vulnerability. Thanks again to @haxatron (#3019)
* Updated translations with latest changes from Crowdin. (#3014)

[1.15.3]
* Update BookStack to 21.10.3
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.10.3)
* Fixed path image file path traversal vulnerability. Thanks @theWorstComrade for reporting. (#3030)
* Prevented HTML attachments being served inline. Thanks @theWorstComrade for reporting. (#3027)
* Updated translations from latest Crowdin changes. (#3023)

[1.16.0]
* Update BookStack to 21.11
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.11)

[1.16.1]
* Update BookStack to 21.11.1
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.11.1)
* Added custom command support to the logical theme system. (#3072)
* Added support for prefers-contrast media setting to increase contrast in faded areas when active. (#2634)
* Updated TOTP confirmation view to autofocus on code input. Thanks to @raccettura. (#3068)
* Updated translations with latest changes from Crowdin. (#3057)
* Updated any links on homepage lists to be more obvious & accessible. (#3046)
* Fixed faulty page navigation links when headers are nested within other content. Thanks to @Julesdevops. (#3069, #3058)

[1.16.2]
* Update BookStack to 21.11.2
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.11.2)
* This is a security release that address a couple of vulnerabilities relating to API access and page draft related content visibility
* Fixed issue with greater-than-expected visibility on page-draft-related items. Thanks @Haxatron for reporting. (#3086)
* Fixed issue where public API access was not limited by system public control in certain conditions. (#3091)

[1.16.3]
* Update BookStack to 21.11.3
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.11.3)
* This is a security release that helps prevent potential discovery and harvesting of user details including name and email address.
* Helped prevent discovery and harvesting of user information. Thanks @Haxatron for reporting. (#3108)
* Updated search API results to include the highlighted preview content. (#3096)
* Updated search API results to include item URL. (#3080)

[1.17.0]
* Update BookStack to 21.12
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.12)
* Added webhooks. (#147, #3099)
* Added ability to copy books, chapters & roles. (#3118, #1123)
* Added audit log IP address search. Thanks to @johnroyer. (#3081)
* Updated translations with latest Crowdin changes. (#3117)
* Fixed issue where non-ascii content could break search result previews. Thanks to @Kristian-Krastev. (#3113)
* Fixed mismatched password validation rules across the application. (#2237)

[1.17.1]
* Update BookStack to 21.12.1
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.12.1)
* Security Release

[1.17.2]
* Update BookStack to 21.12.2
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.12.2)
* Improved handling of uploaded images when thumbnails fail to load. (#3142)
* Updated translations with latest Crowdin changes. (#3148)
* Fixed issue where webhooks would error for specific recycle bin operations. (#3154)
* Fixed Spanish invite email subject translation. Thanks to @AitorMatxi. (#3153)
* Fixed issue where custom homepage could cause strange deletion behavior and lead to errors. (#3150)

[1.17.3]
* Update BookStack to 21.12.3
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.12.3)
* Updated user creation flow to not persist the user on invitation sending failure. Thanks to @Julesdevops. (#3179, #3174)
* Updated "Recently Updated Pages" view to show update author and date. Thanks to @Julesdevops. (#3177, #3045)
* Updated translations with latest Crowdin changes. (#3158)
* Updated PDF page export image display to help fix image sizing issues again. (#3120)
* Updated "Recently Updated Pages" view to show parent context chain. (#3183)
* Fixed potential errors in revision diff view when multi-byte characters are used. (#3170)
* Fixed duplicate display in image gallery when uploading multiple images at once. (#3160)
* Fixed inaccurate markdown editor cursor position upon sidebar usage. (#3186)

[1.17.4]
* Update BookStack to 21.12.4
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.12.4)
* Added --external-auth-id option to the bookstack:create-admin command for use with LDAP/SAML2/OIDC instances. (#3222)
* Added the ability select preferred language when creating a new user. (#2408, #2576)
* Added configuration option for PDF export page size. (#995)
* Updated 503 error view to simplify and prevent thrown errors. Thanks to @Julesdevops. (#3210, #3205)
* Updated translations with latest Crowdin changes. (#3214)
* Fixed mis-represented default registration role and allowed disabling of this option. (#3220, #2338)
* Fixed OIDC autodiscovery when keys are provided in a certain format, as provided by Azure. (#3206)

[1.17.5]
* Update BookStack to 21.12.5
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v21.12.5)
* Added text for "file" validation messages to provide better responses in Attachment API validation failures. (#3248)
* Fixed WYSIWYG editor code block creation across mulitple lines and block elements. Thanks to @Julesdevops. (#3246, #3200)
* Fixed markdown image data URI extraction failing on large images due to regex match limits. (#3249)
* Updated translations with latest Crowdin changes. (#3225)

[1.18.0]
* Update BookStack to 22.02
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v22.02)
* Added collapsible content blocks support to the WYSIWYG editor. (#78, #3260)
* Added translation support to the WYSIWYG editor. (#1838)
* Added user management API endpoints. (#3238, #1363, #2701)
* Changed minimum PHP version from 7.3 to 7.4. (#3245, #3152)
* Updated translations with latest Crowdin changes. (#3258, #3251, #3259)
* Updated Korean translations. Thanks to @ististyle. (#3256)
* Updated TinyMCE WYSIWYG editor to the latest version. (#3247)
* Improved PDF export rendering of images within tables. (#3190)
* Fixed potential web console error message when loading the editor. (#2461)
* Fixed issue where OIDC token failures would not be shown to the user. (#3264)
* Fixed issue where the editor could jump-scroll to the top after format change on FireFox (#2692)

[1.18.1]
* Update BookStack to 22.02.1
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v22.02.1)
* Updated editor references to avoid caching issue that would prevent WYSIWYG editor from opening. (#3293)
* Updated code blocks within the editor to be more reliable, especially on first insertion. (#3292)
* Updated translations with latest changes from Crowdin. (#3291)

[1.18.2]
* Update BookStack to 22.02.2
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v22.02.2)
* Added cache breaker to WYSIWYG onward loading to prevent plugin errors appearing if cached. (#3303)
* Updated translations with latest Crowdin changes. (#3301)
* Updated sidebar fade to be more subtle when in dark mode. (#3203)
* Fixed WYISWYG editor issue where blank lines would collapse. (#3302)

[1.18.3]
* Update BookStack to 22.02.3
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v22.02.3)
* Added iframe allow-list control to prevent a range of malicious uses of untrusted iframe sources. (#3314)
* Updated translations with latest Crowdin changes. (#3312)

[1.19.0]
* Update BookStack to 22.03.1
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v22.03.1)
* Added support for checkbox tasklists in the WYSIWYG editor. (#3333, #4)
* Added WYSIWYG control to remove & edit links. (#3276, #3298)
* Added WYSIWYG Ctrl+Shift+K shortcut to show entity selector popup shortcut in WYSIWYG editor. (#3244, #3298)
* Added LDAP user group debugging option. (#3345)
* Added support for the Basque language. (#3296)
* Updated settings view with a re-organized layout for a less confusing user experience. (#3349, #3221)
* Updated code block rendering in WYSIWYG to help prevent scroll jumping upon undo/redo. (#3326)
* Updated translations with latest Crowdin updates. (#3320)
* Updated webhook data to include details of page/chapter/shelf/book creator/updater/owner. (#3279)
* Updated webhook data to include revision details on page_update and page_create events. (#3218)
* Fixed lack of translation support for some editor buttons. (#3342)
* Fixed incorrect page concatenation in book markdown export. (#3341)
* Fixed usage of <br> tags within code blocks instead of newlines when using the WYSIWYG editor. (#3327)
* Fixed image thumbnail generation not taking EXIF rotation data into account. (#1854)
* Fixed issue where /settings redirect would lead to wrong location in some scenarios. (#3356)
* Fixed non-active prevention of custom HTML head content on settings views. (#3355)
* Updated translations with latest Crowdin changes. (#3354)
* Updated project PHP dependencies.

[1.20.0]
* Update BookStack to 22.04
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v22.04)
* Database Changes - This release makes some significant changes to data within the database which may cause the update to take a little longer than usual to run. Please give the update extra time to complete.
* REST API Page Create/Update Changes - Create & update page requests now have the potential to change the current editor type for that page, depending on the content type sent in the request, if the API user has permission to change the page editor.
* URL Handling - The way we handle URLs has changed this release to hopefully address some issues in specific scenarios. These changes have been tested and should not affect existing working environments but there's an increased risk this release for setups with more complex URL handling. Please raise an issue or jump into our Discord server if you have any issues with URLs after upgrading.

[1.20.1]
* Update BookStack to 22.04.1
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v22.04.1)
* Fixed issue where a duplicate slash could occur in the URL leading to a 404 page. (#3404)
* Updated translations with latest changes from Crowdin. (#3402)

[1.20.2]
* Update BookStack to 22.04.2
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v22.04.2)
* Added Persian to language list. (#3426)
* Updated API docs to detail rate-limit information. (#3423)
* Updated translations with latest Crowdin changes. (#3418)
* Fixed broken attachment downloads in environments where PHP output buffering is disabled. (#3415)

[1.20.3]
* Update BookStack to 22.06
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v22.06)
* Added ability to convert chapters to books, and books to shelves. (#3499, #1087)
* Added ability to use commas in the role "External Auth ID". (#3416, #3405)
* Added body-start/end templates as a convenience to theme system users. (#894)
* Added OCaml to the code editor language list and fixed highlighting type. (#3511)
* Added TypeScript to the code editor language list. (#3494)
* Added common audio types to our WebSafeMimeSniffer for non-download attachment usage. (#3485)
* Added LaTex to the code editor language list. (#3458)
* Updated the UI/design with a mass of fixes & improvements. (#3433)

[1.20.4]
* Update BookStack to 22.06.1
* [Release announcement](https://github.com/BookStackApp/BookStack/releases/tag/v22.06.1)
* Updated entity-selector-popup to reset state upon successful selection. (#3528)
* Updated translations with latest CrowdIn changes. (#3526)
* Fixed non-translated settings category options. (#3529)
* Fixed issue where tags would not be saved upon book update. (#3527)
* Fixed long code in "Custom Head" setting breaking page layout. (#3523)

[1.20.5]
* Update BookStack to 22.06.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.06.1)
* Updated translations with latest CrowdIn changes. (#3540, #3531)
* Fixed bug causing LDAP/SAML2 group mapping to fail if the "External Auth Ids" role field contained upper case characters. (#3535)
* Fixed differing behaviour, between select button and double-click, in the link selector popup. (#3534)

[1.21.0]
* Update BookStack to 22.07
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.07)
* Added 'Sort Book' action to chapters. (#3598, #2335)
* Added ability to favourite code languages in the WYSIWYG code editor. (#3593, #3542)
* Added option to set IP address storage precision. (#3560)
* Added tag-based css classes to the HTML body tag for tag-based content CSS targeting. (#3583)
* Added new Logical Theme System event, emitted upon any system activity event. (#3572)
* Added editor shortcuts for bullet and numbered lists. (#3599, #1269)
* Updated shelf book management interface with better usability and book search bar. (#3591, #3266)
* Updated translations with latest changes from Crowdin. (#3600, #3545)
* Updated WYSIWYG editor to TinyMCE 6. (#3580, #3517)
* Updated DOMPDF, and other PHP dependencies. (#3579)
* Updated permission system to only "cache" view-based permissions for better performance, and made many other performance improvements. (#3569)
* Updated WYSIWYG color options to have no names, for better cross-language usage. (#3530)
* Updated tests to use ssddanbrown/asserthtml library. (#3519)
* Fixed comment count translation in Chinese translations. Thanks to @GongMingCai. (#3556)
* Fixed issue where AVATAR_URL=false would not properly disable Gravatar fetching. (#1835)
* Fixed some German translation typos and grammar. Thanks to @smartshogu. (#3570)
* Fixed issue where WYSIWYG toolbar would remain when after inserting a drawing. (#3597)

[1.21.1]
* Update BookStack to 22.07.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.07.1)
* Fixed issue where old WYSWYG editor code would be cached, preventing the editor from showing. (#3611)
* Updated translations with latest Crowdin changes. (#3605)

[1.21.2]
* Update BookStack to 22.07.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.07.2)
* Added body-start/end partials to export template, for easier export customization via the visual theme system. (#3630)
* Added activity recording for revision delete/restore. (#3628)
* Updated translations with latest changes from Crowdin. (#3625)
* Updated user validation with sensible limit to name input. (#3614)
* Fixed issue where activity type could not be selected in the audit log. (#3623)
* Fixed possibility of breaking page load due to bad user language input. (#3615)

[1.21.3]
* Update BookStack to 22.07.3
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.07.3)
* This is a security release that adds additional filtering to page content to prevent certain cross-site-scripting techniques
* Added API documentation section to advise of content security. (#3636)
* Updated Persian translations. Thanks to @samadha56. (#3639)
* Updated code block rendering to help prevent blank blocks on fresh cache. (#3637)
* Updated HTML filtering to prevent SVG animate case. (#3636)

[1.22.0]
* Update BookStack to 22.09
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.09)
* Security - This release cycle contained a security release that added detail that's important to consider when BookStack content is used externally. See the v22.07.3 post for more detail.
* Revision Visibility - This update fixes a permission disparity with revisions. Revision content has always been accessible to those with page-view permissions, but the links to the revisions list previously required page-edit permission to show. This has been aligned, which may mean page revision links may now show to those that did not previously see them.
* Revision Limit Change - The default, per-page, revision limit has been doubled from 50 to 100, to account for new system-content updates that may occur. If desired, you can configure this to a custom value.
* Reference Index - New features have been added to track links between content in BookStack, which uses an internal reference index. Upon upgrade from an older BookStack version, this index will need to be rebuilt. This can be done with the "Regenerate References" command or via the "Regenerate References" maintenance action within BookStack.

[1.22.1]
* Update BookStack to 22.09.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.09.1)
* Added PHPCS for project PHP formatting. (#3728)
* Updated SAML error handling to display additional error detail. (#3731)
* Updated translations with latest Crowdin updates. (#3710)

[1.23.0]
* Update BookStack to 22.10
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.10)
* Added Greek language. (#3732)
* Added MATLAB code syntax highlighting. (#3744)
* Added toolbar for code blocks in WYSIWYG editor to make mobile editing possible. (#2815)
* Updated content permissions interface & logic to allow more selective/intuitive control. (#3760)
* Update WYSIWYG table toolbar icons to be a little more legible. (#3397)
* Updated auth controller components to not depend on older Laravel library. (#3745, #3627)
* Updated book copy behaviour to copy book-shelf relations if permissions allow. (#3699)
* Updated books-read API endpoint to list child book/chapter tree. (#3734)
* Updated list style handling to align deeply nested list styling in & out of editor. (#3685)
* Updated shelf book management for easier touch device usage. (#2301)
* Updated tag suggestions to provide more accurate results. (#3720)
* Updated testing to support parallel running. (#3751)
* Updated tests to align/clean-up certain common actions. (#3757)
* Updated translations with latest Crowdin changes. (#3737)
* Fixed custom code block theme not used within the WYSIWYG editor. (#3753)
* Fixed issue where revision delete control would show to those without permission. (#3723)
* Fixed justified text not applying to list content. (#3750)
* Fixed not being able to deselect "Created/Update by me" search options. Thanks to @Wertisdk. (#3770, #3762)
* Fixed page popover being hidden behind content in chromium-based browsers. (#3774)
* Fixed SAML2 metadata display depending on external IDP metadata page. (#2480)
* Fixed squashing of columns in users list. (#3787)

[1.23.1]
* Update BookStack to 22.10.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.10.1)
* Fixes issue with generation permissions where a chapter is in the recycle bin.

[1.23.2]
* Update BookStack to 22.10.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.10.2)
* Updated translations with latest changes from Crowdin (#3791)

[1.24.0]
* Update BookStack to 22.11
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.11)
* Added user interface shortcuts system. (#3830, #1216)
* Added global search live preview. (#3850)
* Added markdown preview pane resize/hide/sync controls. (#2215)
* Added Dart/Flutter support for code blocks & editor. (#3808)
* Added Swift language support for code blocks & editor. (#3847)
* Added login/register message partials for easier use via theme system. (#3848, #608)
* Added Georgian Language support on Crowdin. (#3823)
* Updated all interface tabular list views to new format with added functionality. (#3821)
* Updated markdown codebase to be modular and tidied some styles. (#3875)
* Updated dark mode styles with fixes and browser color scheme support. (#3878)
* Updated email confirmation routes to be confirmed via POST. (#3797)
* Updated JavaScript usage to align on single cleaned-up component system. (#3853)
* Updated our testing process to ensure PHP8.2 Support. (#3852)
* Updated tests to cover issue of permission regeneration with chapter in the recycle bin. (#3796)
* Updated translations with latest Crowdin changes. (#3828)
* Fixed app logo not being stored for public access when using "local_secure_restricted" images. (#3827)
* Fixed missing translations for some editor elements. (#3822)
* Fixed OIDC JWKs parsing when "use" property missing on keys. (#3869)

[1.24.1]
* Update BookStack to 22.11.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v22.11.1)
* Added smarty and twig template code language support. Thanks to @jhit. (#3879)
* Updated translations with latest Crowdin changes. (#3881)
* Fixed global search focus issue with arrow keys. (#3920)
* Fixed lack of scroll in editor sidebar views. (#2887)

[1.25.0]
* Update base image to 4.0.0

[1.26.0]
* Update BookStack to 23.01
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.01)
* Permission Changes - There have been changes to the permission system which can affect how permissions apply and therefore could lead to changes in provided abilities upon upgrade. This is only really relevant to complex permission scenarios that have only been possible since BookStack v22.10.
* Added ability to control app icon (favicon) via settings. (#3994, #3929, #301)
* Added ability to set separate colors for dark mode. (#2314, #4002)
* Added ability to set separate colors for primary color and links. (#3910, #4002)
* Added accessible controls to book sorting & improved user experience. (#3999, #3987)
* Added Scheme code highlight support. (#3954)

[1.26.1]
* Update BookStack to 23.01.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.01.1)
* Updated pdf library to address vulnerability. (#4010)
* Updated translations with latest Crowdin changes. (#4008)
* Fixed missing default 180px icon. (#4006)

[1.27.0]
* Update BookStack to 23.02
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.02)
* Added user roles API endpoints. (#4051, #4034)
* Added configuration option for the sendmail command. (#4001)
* Added sort actions and accessible controls to the shelf book management interface. (#4049, #4031, #2050)
* Updated framework to Laravel 9. (#4021, #3123)
* Updated project minimum supported PHP version from 7.4 to 8.0.2. (#4029)
* Updated the URL length limit for link attachments to 2k characters. (#4044)
* Updated app icon handling to generate favicon.ico file where possible. (#4032)
* Updated setting loading to be more efficient. (#4062)
* Updated test handling with cleaner centralized filed/image handling. (#3995)
* Updated translations with latest Crowdin changes. (#4025)
* Fixed issue where uploaded images would not show in the gallery for draft pages. (#4028)
* Fixed issue with increasing WYSIWYG editor lag as pages grow. (#3981)
* Fixed potential pluralization issues in some languages. (#4040)
* Fixed slow response time when saving page due to URL parsing and handling. (#3932)

[1.27.1]
* Update BookStack to 23.02.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.02.1)
* Fixed an issue with language loading in certain scenarios. (#4068)
* Updated translations with latest Crowdin changes. (#4066)

[1.27.2]
* Update BookStack to 23.02.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.02.2)
* Fixed role deletion failing when submitting with empty migration role. (#4128)
* Fixed ownership migration upon user delete not working. (#4124)
* Updated translations with latest Crowdin changes. (#4074)

[1.27.3]
* Update BooktStack to 23.02.3
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.02.3)
* Fixed issue where user delete fails when no "migration" user is selected. (#4162)
* Fixed tag selection via mouse on Safari. (#4139)
* Updated translations with latest Crowdin changes. (#4131)

[1.28.0]
* Update BookStack to 23.05
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.05)
* Added system CLI for admin operations. (#4206, #3149)
* Added image gallery API Endpoints. (#4103)
* Added content permission API endpoints. (#2702, #4099)
* Added new logical theme event to customize OIDC ID token data. (#4200)
* Added Clojure syntax highlighting for code blocks. (#4112)
* Added option to disable SSL verification with SMTP email sending. Thanks to @vincentbernat. (#4126, #3166)
* Added support for three-levels of nested include tags. Thanks to @jasonF1000. (#4192, #2845)
* Added detailed documentation for public JS events. (#4179)
* Added standard JS codebase formatting via ESLint. (#4181, #4180)
* Updated code blocks & markdown editor to CodeMirror 6. (#3617, #3518)
* Updated file upload handling for images and attachments. (#4193)
* Updated SAML2 SLO requests to include a session index. (#3936)
* Updated translations with latest Crowdin changes. (#4163)
* Fixed audit log type filter leading to wrong location. (#4201)
* Fixed large videos within content escaping content area. Thanks to @chopin2712. (#4204)
* Fixed missing WKHTMLTOPDF in .env.example.complete file. Thanks to @7nohe. (#4145)
* Fixed not being able to search for terms containing backslashes . Thanks to @esakkiraja100116. (#4202, #4175)
* Fixed timestamp in API docs example chapter response. Thanks to @tigsikram. (#4191)

[1.28.1]
* Update BookStack to 23.05.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.05.1)
* Updated "update-url" command to allow running non-interactively. (#4223)
* Updated translations with latest Crowdin changes. (#4211)
* Updated WYSWIYG code editor focus handling to more accurately return to editor. (#4109)
* Fixed code block formatting in print/export views. (#4215)
* Fixed extra spacing being added around horizontal rules within collapsible blocks within the WYSIWYG editor. (#3963)
* Fixed "Custom HTML Head Content" style blocks not being used for code blocks within the WYSWIYG editor. (#4228)
* Fixed UI shortcuts being incorrectly active within code blocks. (#4227)

[1.28.2]
* Update BookStack to 23.05.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.05.2)
* Updated view-only code block line highlighting to only show on focus. (#4254)
* Updated System CLI. (#4252)
* Fixed issues regarding symlinked folders for backup and restore.
* Fixed incorrect app directory searching.
* Updated image/attachment file upload buttons to allow selection of mulitple files. (#4241)
* Updated translations with latest Crowdin changes. (#4239)
* Updated attachment drag handling so they can be dragged via their name/link. (#591)

[1.29.0]
* Log external client IP address
* Set trust proxy for bookstack to get the correct client IP

[1.30.0]
* Update BookStack to 23.06.0
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.06)
* Added visual comment threading. (#4286, #3400)
* Added read-only comments listing into page editor. (#4322)
* Added methods for screen-reader/keyboard-only users to use the page section popup. (#3975)
* Added option to delete the current page draft. (#3927)
* Added text for each activity type so that webhooks always have readable text. (#4216)
* Updated image manager with new design to be responsive and more accessible. (#4265)
* Updated how fonts are defined for easier CSS customization. (#4302, #4307)
* Updated pages API to provide raw html in single page responses. (#4310)
* Updated system status colors with dark variants and to be CSS variables for easier customization. (#4301)
* Updated API docs with multi-paragraph descriptions for endpoints. (#4332)
* Updated ldap_connect usage to avoid deprecated syntax. (#4274)
* Updated MAIL_ENCRYPTION options & guidance for clarity. (#4342)
* Updated command codebase to align logic. (#4262, #4225)
* Updated default page copy/move view to show the parent book of chapter targets. (#4264)
* Updated export styles to remove rules redundant for HTML/PDF exports. (#4303)
* Updated JsonDebugException to use the "Responsable" interface. Thanks to @devdot. (#4318)
* Updated shelf permissions view to not show the non-used "create" permission. (#2690)
* Updated translations with latest Crowdin changes. (#4256)
* Updated WYSIWYG editor library from TinyMCE 6.3.1 to 6.5.1.
* Fixed API chapter update not using "book_id" parameter. (#4272)
* Fixed API returns responses to return 404 instead of 500 on not found. Thanks to @devdot. (#4290, #4291)
* Fixed created/updated times not showing using the Romanian language. (#4297)
* Fixed guest user role handling so they can accept custom permissions from other roles. (#1229)
* Fixed inaction when certain parameters were combined using the content-permissions API. (#4323)
* Fixed incorrect times in Users list API. (#4325)
* Fixed misaligned case-sensitive sorting in shelves. (#4341)
* Fixed misaligned date and time format returned by the image gallery API. (#4294)
* Fixed growing table rows in the WYSIWYG when using Firefox. (#4337)

[1.30.1]
* Update BookStack to 23.06.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.06.1)
* Updated MAIL_ENCRYPTION usage due to incorrectly forcing initial TLS usage. (#4358)
* Updated translations with latest Crowdin changes. (#4352)
* Fixed image updated timestamp not updating when gallery images are replaced. (#4354)
* Fixed sort options breaking roles page load. (#4350)
* Fixed IPv6 addresses in audit log spilling into date column. (#4349)
* Fixed many inaccuracies in API example responses. Thanks to @devdot. (#4344)

[1.30.2]
* Update BookStack to 23.06.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.06.2)
* Re-added shelf create permissions, now include a note to indicate permission usage. (#4375)
* Fixed issue causing some delete-based action webhooks to create not-found errors. (#4373)
* Updated translations with latest Crowdin changes. (#4367)

[1.31.0]
* Update BookStack to 23.08.0
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.08)
* Security - Webhooks - In scenarios where admin users are not trusted, webhooks could potentially be used maliciously. This update adds a control for such functionality. Please read our [documentation](https://www.bookstackapp.com/docs/admin/security/#server-side-request-allow-list).
* Added content notification system. (#4390, #4371, #241)
* Added browser-based drawing backup storage mechanism. (#4457, #4421)
* Added order/priority control within books via the API. Thanks to @rouet. (#4313, #4298)
* Added host allow list option for server side requests like webhooks. (#4410)
* Added additional comment-specific activities. (#4389)
* Updated translations with latest Crowdin changes. (#4380, #4462)
* Fixed API docs caching failure when using DB cache driver. (#4453)
* Fixed overly wide page view when using an RTL language. (#4429)
* Fixed status cache check to work better for simultaneous requests. (#4396)
* Fixed markdown editor scrolling on mobile screen sizes. (#4466)

[1.31.1]
* Update BookStack to 23.08.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.08.1)
* Updated preferences view styles to better respond to content and screen sizes to prevent wrapping buttons. (#4502)
* Updated WYSIWYG editor filtering to help prevent page pointer being pasted into pages. (#4474)
* Updated translations with latest Crowdin changes. (#4481)
* Fixed a range of typos in our dev docs. Thanks to @omahs. (#4484)
* Fixed deleted watched books/chapters/pages breaking notification preferences view from loading. (#4499)
* Fixed notifications not being sent in receiver language preference. (#4497, #4480)

[1.31.2]
* Update BookStack to 23.08.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.08.2)
* Fixed WYSIWYG filtering issue, introduced in v23.08.1, which breaks page editing and drawing use when certain elements exist in page content. (#4510, #4509)
* Updated translations with latest Crowdin changes. (#4506)

[1.31.3]
* Move to OIDC login

[1.32.0]
* Move to OIDC login

[1.33.0]
* Move to OIDC login
* Add user migration logic for old installations

[1.33.1]
* Update BookStack to 23.08.3
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.08.3)
* Fixed comment reply notifications not being sent to the correct/expected user. (#4548)
* Fixed JavaScript error that could appear when not having comment permissions. (#4531)
* Fixed wrong French translation in notification preferences. (#4511)
* Updated translations with latest Crowdin changes. (#4512)

[1.34.0]
* Update base image to 4.2.0

[1.35.0]
* Update BookStack to 23.10
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.10)
* Added new "My Account" area. (#4615)
* Added Uzbek language translations. Thanks to @mrmuminov. (#4527)
* Added artisan command for re-fetching existing user avatar images. Thanks to @MarcHagen. (#4560, #1893)
* Added basic PWA support. Thanks to @GamerClassN7. (#4430, #1253)
* Added new header bar partials for easier customization. (#4564)
* Added "View Tags" button to non-default homepage views. (#4558)
* Updated page editor interface with a new design. (#4604)
* Updated app caching behaviour to avoid expiry scenarios. (#4600)
* Updated cleanup-images command to allow non-interactive running. (#4541)
* Updated comment notification options to only show if comments active. Thanks to @tusharnain4578. (#4552, #4508)
* Updated editor entity selector to pre-fill with selected text. (#4571)
* Updated file & image upload handling for better indication of issues. (#4578, #4454)
* Updated guest user logic to reduce complexity and overlapping methods. (#4554, #4448)
* Updated HTTP calling in the codebase to align all handling. (#4525)
* Updated icon handling to remove unneeded global helper. (#4553)
* Updated language handling to reduce complexity and duplicated logic. (#4555, #4501)
* Updated logical theme system to capture load errors for better reporting & debugging. (#4504)
* Updated mixed entity endpoints to share and align logic. (#4444)
* Updated OIDC config handling to move logic out of config file. (#4494)
* Updated OIDC request timeout to 5 seconds. (#4397)
* Updated older notifications codebase to align with newer code organisation. (#4500)
* Updated print view to ignore extra elements. (#4594)
* Updated Slack authentication to use official Laravel implementation. (#4464)
* Updated the default email settings to use example domain. (#4518)
* Updated translations with latest Crowdin changes. (#4523)
* Updated username truncation to always show some part of the name. Thanks to @Bajszi97. (#4533, #4489)
* Updated security docs to remove huntr references. Thanks to @radiantwave. (#4616, #4618)
* Fixed awkward sidebar scroll behaviour at mid-level screen sizes. Thanks to @LawssssCat. (#4562)
* Fixed buggy dark/light mode button when dark mode is the default. (#4543)
* Fixed enter press incorrectly clearing tag input field. (#4570)
* Fixed issue where "?" would show shortcuts when typing in an input. (#4606)
* Fixed lack of content in plaintext export options. (#4557)
* Fixed missing notification text in German-language emails. (#4567)
* Fixed odd default homepage layout at iPad-like width. (#4596)
* Fixed un-aligned text across elements when they show their empty states. (#4563)
* Enabled Albanian translations for BookStack on Crowdin. (#4065)
* Enabled Finnish translations for BookStack on Crowdin. (#4614)
* Enabled Norwegian Nynorsk translations for BookStack on Crowdin. (#4447)

[1.35.1]
* Update BookStack to 23.10.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.10.1)
* Added "Norwegian Nynorsk" to user langauge options.
* Added JavaScript public event for customizing codemirror instances. (#4639)
* Added handling to allow jumping to headers/sections within collapsible sections. (#4637)
* Added PHP 8.3 support. (#4633)
* Updated translations with latest Crowdin changes. (#4631)
* Fixed header bar peeking through on markdown editor fullscreen mode. (#4641)
* Fixed incorrect color usage for editor toolbox active tabs. (#4630)

[1.35.2]
* Update BookStack to 23.10.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.10.2)
* Fixed incorrect audit log dropdown behaviour. (#4652)
* Fixed redirects to the manfiest endpoint in some environments. (#4649)
* Updated translations with latest Crowdin changes. (#4643)

[1.36.0]
* Symlink themes to `/app/data/themes` . This allows creation of custom themes

[1.36.1]
* Update BooktStack to 23.10.4
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.10.4)
* Updated thumbnail handling to for use of content as image data. (#4681)

[1.37.0]
* Update BooktStack to 23.12
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.12)
* Added simple WYSIWYG for description fields. (#4729, #2354, #2203)
* Added default template option for books. Thanks to @lennertdaniels. (#4721, #3918, #1803)
* Added OIDC RP-initiated logout. Thanks to @joancyho. (#4714, #4467, #3715)
* Added new Logical Theme System event to register web routes. (#4663)
* Updated email notifications to include the page parent chapter/book. Thanks to @Man-in-Black. (#4629)
* Updated and standardised DOM handling in the codebase. (#4673)
* Updated back redirection handling to not rely on referrer headers. (#4656)
* Updated book/chapter/shelf description character limit. (#4085)
* Updated design of buttons to be a bit friendlier. (#4728)

[1.37.1]
* Update BookStack to 23.12.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.12.1)
* Fixed chapter API missing expected "book_slug" field. (#4765)
* Updated translations with latest Crowdin changes. (#4747)

[1.37.2]
* Update BookStack to 23.12.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.12.2)
* Fixed attachment list ctrl-click not opening attachments inline. (#4782)
* Updated translations with latest Crowdin changes. (#4779)
* Fixed entity selector popup pre-fill not searching term as expected. (#4778)

[1.37.3]
* Update BookStack to 23.12.3
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v23.12.3)
* This is a security release that addresses a vulnerability in PDF generation that could be exploited to perform blind server-side-request forgery.

[1.38.0]
* Update BookStack to 24.02
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.02)
* Security - The v23.12 branch of BookStack recently had a security release, which you can find details of in our v23.12.3 blogpost.
* Comments - The ability to use markdown content in comments has been removed in this release, replaced by a WYSIWYG editor. Markdown in comments was a fairly hidden feature though so was not commonly utilised. Existing markdown comments will remain although formatting may be lost if old markdown comments are edited.
* Commands - The "Regenerate Comment Content" command has been removed in this release since this action is now redundant.
* OIDC Authentication - Proof Key for Code Exchange (PKCE) support has been added to BookStack OIDC authentication. This should not affect existing OIDC use but you may want to enforce PKCE to be required for BookStack on your authentication system, if supported, for extra security.
* Added simple WYSIWYG comment editor inputs. (#4815, #3018)
* Added default page templates for chapters. Thanks to @Man-in-Black. (#4750, #4764)
* Added PKCE support for OIDC. (#4804, #4734)
* Added "Clear table formatting" & "Resize to contents" WYSIWYG table options. (#4845)
* Added "Toggle header row" button to table toolbar in WYSWIYG editor. (#985)
* Added attachment serving range request support. (#4758, #3274)
* Added new AUTH_PRE_REGISTER logical theme event. (#4833)
* Updated app entity loading to be more efficient and avoid global addSelects. (#4827, #4823)
* Updated book/shelf cover image wording to make sizing in usage clearer. (#4748)
* Updated PWA manifest to allow landscape use. Thanks to @shashinma. (#4828)
* Updated redirect handling to reduce chance of redirecting to images. (#4863)
* Updated some EN text for consistency/readability. (#4794)
* Updated WYSIWYG editor with improved cell selection formatting clearing. (#4850)
* Updated WYSIWYG text direction & alignment controls to work more reliably on complex structures. (#4843)
* Fixed breadcrumb dropdowns being partially out of view on mobile screen sizes. (#4824)
* Fixed description WYSIWYG not respecting RTL text. (#4810)
* Fixed header bar collapse on smaller screen sizes when no name or logo is used. (#4841)
* Fixed incorrect pagination display in RTL layout. (#4808)
* Fixed JavaScript error logged on WYSIWYG editor load due to how custom styles were imported. (#4814)
* Fixed scrollbars showing on WYSIWYG table cell range selection in some browsers. (#4844)
* Fixed WYSIWYG code block text direction controls not being respected. (#4809)

[1.38.1]
* Update BookStack to 24.02.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.02.1)
* Updated translations with latest Crowdin changes. (#4877)
* Updated breadcrumb book & shelf lists to be name-ordered. (#4876)
* Updated MFA inputs to avoid auto-complete. Thanks to @ImMattic. (#4849)
* Fixed non-breaking spaces causing combined words in page navigation. (#4836)
* Fixed page navigation click not jumping to headers in nested collapsible blocks. (#4878)

[1.38.2]
* Update BookStack to 24.02.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.02.2)
* New version to address missed version and asset changes in v24.02.1. (#4889)

[1.38.3]
* Update BookStack to 24.02.3
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.02.3)
* Fixed non-working "Open Link In..." option for description editors. (#4925)
* Fixed failed reference loading when references are from recycle bin items. (#4918)
* Fixed failed code block rendering when a code language was not set. (#4917)
* Updated page editor max content widths to align with page display. (#4916)

[1.39.0]
* Update BookStack to 24.05
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.05)
* Added new command-based PDF export option. (#4969, #4732)
* Added Audit Log API list endpoint. (#4987, #4316)
* Added OIDC userinfo endpoint support. Thanks to @LukeShu. (#4955, #4726, #3873)
* Added simple registration form honeypot. Thanks to @nesges. (#4970)
* Added Scala to list of supported languages in code blocks. (#4953)
* Added licenses page supported by licenses list building process. (#4907)
* Updated app framework from Laravel 9 to 10. (#4903)
* Updated content links to be underlined by default for accessibility. (#4939)
* Updated dev Dockerfile with improvements. Thanks to @C0rn3j. (#4895)
* Updated included images with extra compression to save data. Thanks to @C0rn3j. (#4904)
* Updated JS build system to split markdown-focused packages to own file. (#4930, #4858)
* Updated minimum required PHP version from 8.0 to 8.1. (#4894, #4893)
* Updated translations with latest Crowdin changes. (#4890)
* Fixed code direction in WYSWIYG editor lacking direction support in code editor. (#4943)
* Fixed difference of line-heights for paragraphs in tables between editor and page view. (#4960)
* Fixed extra space at the beginning of a translation. Thanks to @johnroyer. (#4972)
* Fixed failing drag and drop of attachments into editor on Chrome. (#4975)

[1.39.1]
* Update BookStack to 24.05.1
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.05.1)
* Updated PHP dependencies.
* Updated routes with IP-based rate limiting. (#4993)
* Updated email confirmation flow to not require email submission form.
* Updated translations with latest Crowdin changes. (#4994)
* Updated WYSIWYG alignment handling to also consider table align attributes. (#5011)
* Fixed attachment upload validation errors appearing as JSON. (#4996)
* Fixed incorrect notification preferences URL in email. Thanks to @KiDxS. (#5008, #5005)
* Fixed non-visible MFA setup titles in dark mode. (#5018)
* Fixed outdated path in visual theme system guidance. (#4998)
* Fixed potential cache permission issues by reverting cache location. (#4999)

[1.39.2]
* Update BookStack to 24.05.2
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.05.2)

[1.39.3]
* Update BookStack to 24.05.3
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.05.3)
* Updated translations with latest Crowdin changes. (#5065)
* Updated callouts with LTR text handling where supported. (#5104)
* Updated project PHP and JavaScript dependencies.
* Fixed blocked diagrams.net loading when using a custom URL that includes a port. (#5107)
* Fixed OIDC incorrectly calling userinfo endpoint when valid empty groups provided. (#5101)
* Fixed image replacement being case-sensitive when it should not be. Thanks to @DanielGordonIT. (#5096) (#5095)
* Fixed HTML code block highlighting when custom self-closing tags are used. (#5078)

[1.39.4]
* Update BookStack to 24.05.4
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.05.4)
* Security Release
* Updated default content iframe embed max-width to align with other content types. (#5130)
* Updated LDAP group sync to query via full DN.
* Updated translations with latest Crowdin changes. (#5118)
* Fixed books read API response not applying visibility control to chapter contents.

[1.40.0]
* Update BookStack to 24.10
* [Full changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.10)
* Added ability to configure the PDF export command timeout. (#5119)
* Added new Lexical based editor. (#5058)
* Added not operator to search. (#4536)
* Added OpenSearch support. Thanks to @maximilian-walter. (#5198)
* Added SAS and R code language support. (#5206)
* Added search term negation support. (#5239)
* Added Welsh language to language list. (#5240)
* Updated dompdf and bacon-qr-code libraries to new major versions. (#5222)
* Updated page editor type to always exist in API and database. (#5117)
* Updated translations with latest Crowdin changes. (#5188)

[1.40.1]
* Update BookStack to 24.10.1
* [Full Changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.10.1)
* [Update instructions](https://www.bookstackapp.com/docs/admin/updates)
* Updated System CLI with fixes and updated dependencies. ([#&#8203;5312](https://github.com/BookStackApp/BookStack/pull/5312))
* Fixed update-url command not updating revisions & drafts. ([#&#8203;5292](https://github.com/BookStackApp/BookStack/issues/5292))
* Fixed the namespaces of some tests. Thanks to [@&#8203;LordSimal](https://github.com/BookStackApp/BookStack/pull/5291). ([#&#8203;5291](https://github.com/BookStackApp/BookStack/pull/5291), [#&#8203;5071](https://github.com/BookStackApp/BookStack/issues/5071))
* Fixed misaligned user input validation. ([#&#8203;5263](https://github.com/BookStackApp/BookStack/issues/5263))
* Updated setting categories to validate by for existing views, allowing custom categories to be used via the theme system. Thanks to [@&#8203;LachTrip](https://github.com/BookStackApp/BookStack/pull/5255). ([#&#8203;5255](https://github.com/BookStackApp/BookStack/pull/5255), [#&#8203;5251](https://github.com/BookStackApp/BookStack/issues/5251))
* Updated translations with latest Crowdin changes. ([#&#8203;5250](https://github.com/BookStackApp/BookStack/pull/5250))

[1.40.2]
* Update BookStack to 24.10.2
* [Full Changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.10.2)
* [Update details on blog](https://www.bookstackapp.com/blog/bookstack-release-v24-10-2/)
* Updated application PHP dependencies.
* Updated translations with latest Crowdin changes. ([#&#8203;5317](https://github.com/BookStackApp/BookStack/pull/5317))

[1.40.3]
* Update BookStack to 24.10.3
* [Full Changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.10.3)
* [Update instructions](https://www.bookstackapp.com/docs/admin/updates)
* Updated PHP dependency package versions.
* Updated translations with latest Crowdin changes. ([#&#8203;5331](https://github.com/BookStackApp/BookStack/pull/5331))
* Fixed attachment stream handling for better Chrome video support. ([#&#8203;5342](https://github.com/BookStackApp/BookStack/issues/5342), [#&#8203;5088](https://github.com/BookStackApp/BookStack/issues/5088))
* Fixed page include issue caused by PHP 8.3.14 bug. ([#&#8203;5341](https://github.com/BookStackApp/BookStack/issues/5341))
* Fixed OIDC userinfo handling when response included charset content type. Thanks to [@&#8203;wesbiggs](https://github.com/BookStackApp/BookStack/pull/5337). ([#&#8203;5337](https://github.com/BookStackApp/BookStack/pull/5337))
* Fixed differing code line height between dark/light modes. ([#&#8203;5146](https://github.com/BookStackApp/BookStack/issues/5146))

[1.40.4]
* checklist added to CloudronManifest

[1.41.0]
* Update BookStack to 24.12
* [Full Changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.12)
* [Release video overview](https://foss.video/w/imWKrALvSwokP3vrjRdUXh)
* [Update instructions](https://www.bookstackapp.com/docs/admin/updates)
* [Update details on blog](https://www.bookstackapp.com/blog/bookstack-release-v24-12/)
* Added new portable ZIP import/export format. ([#&#8203;5260](https://github.com/BookStackApp/BookStack/pull/5260), [#&#8203;43](https://github.com/BookStackApp/BookStack/issues/43))
* Added support for concatenating multiple LDAP attributes in displayName. Thanks to [@&#8203;MatthieuLeboeuf](https://github.com/BookStackApp/BookStack/pull/5295). ([#&#8203;5295](https://github.com/BookStackApp/BookStack/pull/5295), [#&#8203;1684](https://github.com/BookStackApp/BookStack/issues/1684))
* Added book and chapter titles to search API results. Thanks to [@&#8203;rashadkhan359](https://github.com/BookStackApp/BookStack/pull/5280). ([#&#8203;5280](https://github.com/BookStackApp/BookStack/pull/5280), [#&#8203;5140](https://github.com/BookStackApp/BookStack/issues/5140))
* Added cover image details to book/shelf API list responses. ([#&#8203;5180](https://github.com/BookStackApp/BookStack/issues/5180))
* Updated dev dockerfile setup to simplify things. Thanks to [@&#8203;johnroyer](https://github.com/BookStackApp/BookStack/pull/5293). ([#&#8203;5293](https://github.com/BookStackApp/BookStack/pull/5293))
* Updated guest account form to hide language preference to prevent confusion. ([#&#8203;5356](https://github.com/BookStackApp/BookStack/issues/5356))
* Updated new WYSIWYG editor codebase to merge nodes & re-organise code. ([#&#8203;5349](https://github.com/BookStackApp/BookStack/pull/5349))
* Updated notification handling to not block user with errors on send failures. ([#&#8203;5315](https://github.com/BookStackApp/BookStack/issues/5315))
* Updated our JavaScript service files to TypeScript. ([#&#8203;5259](https://github.com/BookStackApp/BookStack/pull/5259))
* Updated project NPM package & SASS deprecations/changes. ([#&#8203;5354](https://github.com/BookStackApp/BookStack/pull/5354))
* Updated the new WYSIWYG editor with a range of fixes/updates. ([#&#8203;5365](https://github.com/BookStackApp/BookStack/pull/5365))
* Updated translations with latest Crowdin changes. ([#&#8203;5345](https://github.com/BookStackApp/BookStack/pull/5345))
* Fixed API attachment update issue when name not provided. ([#&#8203;5353](https://github.com/BookStackApp/BookStack/issues/5353))
* Fixed attachment actions showing when lacking permissions. ([#&#8203;5323](https://github.com/BookStackApp/BookStack/issues/5323))
* Fixed missing book description and formatting in markdown exports. Thanks to [@&#8203;czemu](https://github.com/BookStackApp/BookStack/pull/5313). ([#&#8203;5313](https://github.com/BookStackApp/BookStack/pull/5313))
* Fixed page indexing breaking with very large pages. ([#&#8203;5322](https://github.com/BookStackApp/BookStack/issues/5322))

[1.41.1]
* Update BookStack to 24.12.1
* [Full Changelog](https://github.com/BookStackApp/BookStack/releases/tag/v24.12.1)
* [Update instructions](https://www.bookstackapp.com/docs/admin/updates)
* Updated export logic to have better temp file clean-up. ([#&#8203;5374](https://github.com/BookStackApp/BookStack/issues/5374), [#&#8203;5379](https://github.com/BookStackApp/BookStack/pull/5379))
* Updated in-app export endpoints to have rate limits. ([#&#8203;5379](https://github.com/BookStackApp/BookStack/pull/5379))
* Updated translations with latest Crowdin changes. ([#&#8203;5370](https://github.com/BookStackApp/BookStack/pull/5370))
* Updated PHP dependency package versions.
* Fixed markdown editor focus jumping on image insert. ([#&#8203;5384](https://github.com/BookStackApp/BookStack/issues/5384))

[1.42.0]
* Update BookStack to 25.02
* [Full Changelog](https://github.com/BookStackApp/BookStack/releases/tag/v25.02)
* [Release video overview](https://foss.video/w/iUZWrk4oxLYc2dFsAPjDyY)
* [Update instructions](https://www.bookstackapp.com/docs/admin/updates)
* [Update details on blog](https://www.bookstackapp.com/blog/bookstack-release-v25-02/)
* Added sort rules with automatic book sorting. ([#&#8203;5457](https://github.com/BookStackApp/BookStack/pull/5457), [#&#8203;2065](https://github.com/BookStackApp/BookStack/issues/2065))
* Added method to serve public files via the theme system. ([#&#8203;5405](https://github.com/BookStackApp/BookStack/pull/5405), [#&#8203;3904](https://github.com/BookStackApp/BookStack/issues/3904))
* Updated app framework to Laravel 11. ([#&#8203;5400](https://github.com/BookStackApp/BookStack/pull/5400))
* Updated codebase minimum PHP version from 8.1 to 8.2. ([#&#8203;5397](https://github.com/BookStackApp/BookStack/issues/5397))
* Updated codebase to address various PHP 8.4 deprecations. ([#&#8203;5491](https://github.com/BookStackApp/BookStack/pull/5491))
* Updated new WYSIWYG editor with a range of fixes. ([#&#8203;5415](https://github.com/BookStackApp/BookStack/pull/5415))
* Updated search indexing to handle guillemets. Thanks to [@&#8203;inv-hareesh](https://github.com/BookStackApp/BookStack/pull/5475). ([#&#8203;5475](https://github.com/BookStackApp/BookStack/pull/5475), [#&#8203;5471](https://github.com/BookStackApp/BookStack/issues/5471))
* Updated search indexing with advanced tokenization along with hyphen handling. ([#&#8203;5488](https://github.com/BookStackApp/BookStack/pull/5488), [#&#8203;2095](https://github.com/BookStackApp/BookStack/issues/2095))
* Updated sort handling to not increment the updated date for sorted content. ([#&#8203;1777](https://github.com/BookStackApp/BookStack/issues/1777))
* Updated translations with latest Crowdin changes. ([#&#8203;5409](https://github.com/BookStackApp/BookStack/pull/5409), [#&#8203;5399](https://github.com/BookStackApp/BookStack/pull/5399))
* Fixed incorrect image orientation handling. ([#&#8203;5462](https://github.com/BookStackApp/BookStack/issues/5462))
* Fixed layout issues at specific breakpoints. ([#&#8203;5396](https://github.com/BookStackApp/BookStack/issues/5396))
* Fixed LDAP error thrown when server does not provide a cn value. ([#&#8203;5443](https://github.com/BookStackApp/BookStack/issues/5443))
* Fixed wrong condition for showing new books list. Thanks to [@&#8203;Silverlan](https://github.com/BookStackApp/BookStack/pull/5470). ([#&#8203;5470](https://github.com/BookStackApp/BookStack/pull/5470))

