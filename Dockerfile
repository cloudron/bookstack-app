FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code/ /app/pkg
WORKDIR /app/code

# the latest composer, not pinned by BookStack
ARG COMPOSER_VERSION=2.7.6
RUN curl --fail -sS https://getcomposer.org/download/${COMPOSER_VERSION}/composer.phar -o /usr/local/bin/composer && chmod +x /usr/local/bin/composer

# renovate: datasource=github-releases depName=BookStackApp/BookStack versioning=regex:^(?<major>\d+)\.(?<minor>\d+)\.?(?<patch>\d+)?$ extractVersion=^v(?<version>.+)$
ARG BOOKSTACK_VERSION=25.02

# get the code and extract it
RUN wget https://github.com/BookStackApp/BookStack/archive/v${BOOKSTACK_VERSION}.tar.gz -O -| tar -xz -C /app/code --strip-components=1 && \
    chown www-data:www-data -R /app/code

RUN sudo -u www-data composer install --no-interaction --prefer-dist --no-suggest --optimize-autoloader --no-dev && \
    sudo -u www-data composer clear-cache

RUN mv /app/code/bootstrap/cache /app/code/bootstrap/cache_orig && ln -s /run/bookstack/bootstrap-cache /app/code/bootstrap/cache && \
    rm -rf /app/code/public/uploads && ln -s /app/data/uploads /app/code/public/uploads && \
    rm -rf /app/code/themes && ln -s /app/data/themes /app/code/themes && \
    mv /app/code/storage /app/code/storage_orig && ln -s /app/data/storage /app/code/storage && \
    ln -s /app/data/env /app/code/.env

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/bookstack.conf /etc/apache2/sites-enabled/bookstack.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite php8.3
RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 128M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_size 128M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP max_execution_time 200

ADD env.production start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
